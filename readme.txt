﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              AlwaysIncludeSubfoldersInSearch
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_1_20120703

*******************************************************************************
** Overview
*******************************************************************************

	This component overrides the dynamichtml include 
	"collection_recursive_search_cb" to ensure that the checkbox 
	"include subfolders" is always checked on the Search form for the 
	Contribution Folders (folders_g) component.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	
	- 10.1.3.5.1 (111229) (Build: 7.2.4.105) 

*******************************************************************************
** HISTORY
*******************************************************************************

	build_1_20120703
		- Initial component release